## Deskripsi

Repo ini berisi projek laravel kosong yang sudah ada template dashboard adminnya (laravel v10)

Bebas untuk menggunakan template yang ada direpo ini.

Silahkan cek branch untuk melihat list template yang tersedia

Salam hangat, Viktor :innocent:

----------------------------------------------------------
**Detail views**
- layouts = folder berisi layouts utama dashboard
- pages = folder berisi semua halaman yang akan dibangun

----------------------------------------------------------
**Tech Stack Detail**
- Laravel v10 (bisa di upgrade atau downgrade)
